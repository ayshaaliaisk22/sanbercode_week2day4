<?php
    require('Animal.php');
    require('Ape.php');
    require('Frog.php');

    $object = new Animal("shaun");

    echo "Name :  $object->name <br>";
    echo "legs : $object->legs <br>";
    echo "cold blooded :  $object->cold_blooded  <br>";

    echo "<br>";

    $object2 = new Frog("buduk");

    echo "Name : $object2->name <br>";
    echo "legs : $object2->legs <br>";
    echo "cold blooded :  $object2->cold_blooded  <br>";
    $object2->jump();

    echo "<br><br>";

    $object3 = new Ape("kera sakti");

    echo "Name : $object3->name <br>";
    echo "legs : $object3->legs <br>";
    echo "cold blooded :  $object3->cold_blooded  <br>";
    $object3->yell();

?>